var 
	allGal = [],
	loadedGallery= [],
	touches = [];

var isMobile = function(){
	if(navigator.userAgent.match(/Android/i)||navigator.userAgent.match(/webOS/i)||navigator.userAgent.match(/iPhone/i)||navigator.userAgent.match(/iPad/i)||navigator.userAgent.match(/iPod/i)||navigator.userAgent.match(/BlackBerry/i)||navigator.userAgent.match(/Windows Phone/i)){
		return true;
	}
	else {
		return false;
	}
}

function cleanUp(value){
	return value.replace(/--/g, '\u2013')
				.replace(/\b'\b/g, "\u2019")
				.replace(/'\b/g, "\u2018")
				.replace(/'é/g,"\u2018é")
				.replace(/'É/g,"\u2018É")
				.replace(/'/g, "\u2019")
				.replace(/"\b/g, "\u201c")
				.replace(/"á/g,"\u201cá")
				.replace(/"é/g,"\u201cé")
				.replace(/"í/g,"\u201cí")
				.replace(/"ó/g,"\u201có")
				.replace(/"ú/g,"\u201cú")
				.replace(/"Á/g,"\u201cÁ")
				.replace(/"É/g,"\u201cÉ")
				.replace(/"Í/g,"\u201cÍ")
				.replace(/"Ó/g,"\u201cÓ")
				.replace(/"Ú/g,"\u201cÚ")
				.replace(/"\[/g,"\u201c[")
				.replace(/"/g, "\u201d")
				.replace(/\b",/g, "\u201d,")
				.replace(/m3/g,'m³')
				.replace(/m2/g,'m²')
				.replace(/O2/g,'O₂')
				.replace(/\u002E\u002E\u002E/g,'…')
				.replace(/&#8747;/g,'"');
}

function corrige(txt) {

	var result = ''

	if(txt.search('<')>=0 && txt.search('>')>=0){
		var text  = txt.split(/[<>]/);
		$.each(text, function(i, val){

			if(i==0){
				result+=cleanUp(val);
			} else if(i!=text.length-1){
				if(isEven(i)){
					result+='>'+cleanUp(val);
				} else {
					result+='<'+val;
				}

			} else {
				result+='>'+cleanUp(val);
			}

		});
	} else {
			result = cleanUp(txt);
	}

	return result;
}

function slug(nome) {
	var from = 'ãàáäâẽèéëêìíïîõòóöôùúüûñç·_,:;/-',
		to = 'aaaaaeeeeeiiiiooooouuuunc';

	nome = nome.toLowerCase().replace(/^\s+|\s+$/g, '').replace(/\s+/g, '').replace('.', '').replace(/'/g, '').replace(/\(/g, '').replace(/\)/g, '').replace(/\[/g, '').replace(/\]/g, '').replace(/\$/g, 's');
	for (var i = 0, l = from.length ; i < l ; i++) {
		nome = nome.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}
	return nome;
}

function imgLoaded($this){
		
		$this.removeClass('lazy');
}



/* INICIO - Galeria - Botão Direito*/
	function dir(){
		if ($('.mod figure.atual').index()-2>=0) {
			$('.mod').find('figure.atual').removeClass('atual');
			$('.mod').find('figure.proximo').removeClass('proximo').addClass('atual');
			$('.mod').find('figure.enterior').removeClass('anterior').addClass('inativo');
			$('.mod').find('figure.atual').next('figure').removeClass().addClass('proximo');
			$('.mod').find('figure.atual').prev('figure').removeClass().addClass('anterior');
			
			omtrHitCounter('fotos', "img url: "+$('.mod').find('figure.atual img').attr('src'));
			window.ivc('trackPageView');
			
			if(	$('.mod figure.atual').index()-2 >= $('.mod').find('figure').length){
				
				$('#dir	').css({"display": "none"})
			}

			if(	$('.mod figure.atual').index()-2 >= 0){
				
				$('#esq	').css({"display": "block"})
			}
		}
	}
/* FINAL - Galeria - Botão Direito*/


/* INICIO - Galeria - Botão Esquerdo*/
	function esq(){
		$('.mod').find('figure.atual').removeClass('atual');
		$('.mod').find('figure.proximo').removeClass('proximo').addClass('inativo');
		$('.mod').find('figure.anterior').removeClass('anterior').addClass('atual');
		$('.mod').find('figure.atual').prev('figure').removeClass().addClass('anterior');
		$('.mod').find('figure.atual').next('figure').removeClass().addClass('proximo');
		omtrHitCounter('fotos', "img url: "+$('.mod').find('figure.atual img').attr('src'));
		window.ivc('trackPageView');
		if(	$('.mod figure.atual').index()-2 <= $('.mod').find('figure').length){
			$('#dir	').css({"display": "block"})
		}
		if(	$('.mod figure.atual').index()-2 <= 1){
			$('#esq	').css({"display": "none"})

		}
	}
/* FINAL - Galeria - Botão Esquerdo*/


/* INICIO - Galeria - Fechar Galeria*/
	function clos(){
		for (var i = 0; i <  $('.mod').find('figure').length; i++) {
			$('.mod').find('figure').removeClass().addClass('inativo');
		};

		$('.mod').removeClass('mod').addClass('modoff');
		$("#topofolha").css({"z-index":'10000'})


	}
/* FINAL - Galeria - Fechar Galeria*/



/* INICIO - Carrega Conteúdo*/
	function loadData(data){

		var abertura = document.createElement('SECTION'),
			linhaFina = document.createElement('P'),
			content = document.getElementById('content'),
			modal = document.createElement('DIV'),
			dir = document.createElement('DIV'),
			esq = document.createElement('DIV'),
			clique = document.createElement('IMG'),
			tituloContainer = document.createElement('DIV'),
			close = document.createElement('DIV'),
			closech1 = document.createElement('DIV'),
			closech2 = document.createElement('DIV'),
			dirb = document.createElement('DIV'),
			esqb = document.createElement('DIV'),
			keys = document.createElement('DIV'),
			outras = document.createElement('DIV'),
		jsoon = document.createElement('SCRIPT');

		



		close.appendChild(closech1);
		close.appendChild(closech2);
		closech1.classList.add('closer');
		closech2.classList.add('closer');
		dir.id='dir';
		esq.id='esq';
		modal.id="modal2"

		keys.innerHTML = 'use as setas para navegar';
		keys.classList.add('info-keys');
		keys.classList.add('icon');
		dirb.classList.add('seta');
		esqb.classList.add('seta');
		close.classList.add('close');
		modal.classList.add('modoff');
		dir.appendChild(dirb);
		esq.appendChild(esqb)
		content.appendChild(keys)
		modal.appendChild(dir);
		modal.appendChild(esq);
		modal.appendChild(close);
		close.id="feche"

		content.appendChild(modal);



		if(data.titulo!= undefined && data.titulo!= 'undefined' && data.titulo!=''){
			var tituloTxt = document.createTextNode(corrige(data.titulo));
			
			abertura.classList.add('abertura');
			tituloContainer.id = 'titulo';

			if(data.tituloImg!=undefined && data.tituloImg!='undefined' && data.tituloImg!=''){
				var titulo = document.createElement('IMG');

				titulo.setAttribute('src', data.tituloImg)
				titulo.setAttribute('alt' ,corrige(data.titulo));
				titulo.setAttribute('title' ,corrige(data.titulo));
				tituloContainer.appendChild(titulo);
			}else {
				var titulo = document.createElement('H1');
				
				titulo.appendChild(tituloTxt);
				tituloContainer.appendChild(titulo);
			}

			abertura.appendChild(tituloContainer);

			if(data.linhaFina!=undefined && data.linhaFina!='undefined' &&data.linhaFina!=''){
				var linhaFinaTxt=document.createTextNode(corrige(data.linhaFina)),
					autor = document.createElement('SPAN'),
					image = document.createElement('IMG'),
					autorTxt = document.createTextNode('');

				image.id="logo";
				image.setAttribute('src','images/logo.svg')
				linhaFina.appendChild(linhaFinaTxt);
				linhaFina.classList.add('linhaFina');
				autor.appendChild(autorTxt)
				content.appendChild(image)
				linhaFina.appendChild(autor)
			}

			clique.classList.add('clique')
			content.appendChild(abertura);
			content.appendChild(linhaFina);
			linhaFina.appendChild(clique);
		}


		if(data.imagens!=undefined && data.imagens.length>0 && data.imagens[0].imagem!=undefined  &&  data.imagens[0].imagem!=''){
			var imageWrapper =  document.createElement('SECTION');

			allImgs = data.imagens;
			imageWrapper.classList.add('imagensa');
			imageWrapper.classList.add('grid-wrap');
			
			for(i=0; i<data.imagens.length; i++){
				var image = document.createElement('FIGURE'),
					imgContainer = document.createElement('DIV'),
					imgreal = document.createElement('IMG'),
					legenda = document.createElement('FIGCAPTION'),
					legendaTxt = document.createTextNode(data.imagens[i].legenda),
					legendam = document.createElement('FIGCAPTION'),
					legendac = document.createElement('P'),
					legendaTxtm = document.createTextNode(data.imagens[i].legenda),
					credito = document.createElement('SPAN'),
					creditoTxt = document.createTextNode(data.imagens[i].credit),
					imagem = document.createElement('FIGURE'),
					imgContainerm = document.createElement('DIV'),
					imgrealm = document.createElement('IMG'),
					leia = document.createElement('A'),
					cont = document.createElement('H6');
					
					cont.innerHTML= (i+1)+"/"+data.imagens.length;
					imagem.appendChild(cont)

				if (data.imagens[i].leiaMais!=undefined && data.imagens[i].leiaMais!='') {
					
					leia.innerHTML=" Leia Mais"
					leia.setAttribute('href', data.imagens[i].leiaMais)
					leia.setAttribute('target', "_blank")
				}
					

				image.id= "imagejj"+i;
				image.classList.add('figure-antes');
				legenda.classList.add('legenda');
				credito.classList.add('credito');

				if (data.imagens[i].orientacao!=undefined && data.imagens[i].orientacao!='') {

					if (data.imagens[i].orientacao=='horizontal') {

						imgContainer.classList.add('img-container');
					}
					
					else if(data.imagens[i].orientacao=='vertical'){

						imgContainer.classList.add('img-container-vert');
					}
					
				}else{

					imgContainer.classList.add('img-container');
				}
						
				if (isMobile()) {
					imgreal.setAttribute('src', data.imagens[i].thumb);
				
				}else{				
					imgreal.setAttribute('src', data.imagens[i].thumb);
				}

				credito.innerHTML="<br> Foto: "
				credito.appendChild(creditoTxt)


				if (data.imagens[i].titulo!=undefined && data.imagens[i].titulo!='') {

					legendac.innerHTML="<b>"+data.imagens[i].titulo +" | </b>"+data.imagens[i].legenda;
				}else{

					legendac.innerHTML=+data.imagens[i].legenda;
				}
						
				
				legendam.appendChild(legendac)
				legendac.appendChild(leia)
				legendac.appendChild(credito)
				imgContainer.appendChild(imgreal);
				image.appendChild(imgContainer);
				imageWrapper.appendChild(image);
				imagem.appendChild(imgContainerm);
				modal.appendChild(imagem);
				imagem.appendChild(legendam);
				imagem.classList.add('inativo');
			}

			content.appendChild(imageWrapper);
			content.appendChild(outras);
		}


		/* Interações*/

		/* clique thumb*/
		$(document).on('click ', '.figure-antes', function(){
			loadGal(this)	

			$('FIGCAPTION').removeClass('hide')
			if ($( window ).width()<=650) {

				$('.info-keys').css({"display":"none"});
			}else{

				$('.info-keys').css({"display":"block"});
			}

			var aa = parseInt(this.id.split('imagejj')[1]);
$('body').css({"overflow":"hidden"})
			$('.atual div').css({"background-image":"url("+data.imagens[aa].imagem+")"})
			$('.proximo div').css({"background-image":"url("+data.imagens[aa+1].imagem+")"})
			$('.anterior div').css({"background-image":"url("+data.imagens[aa-1].imagem+")"})
			

		});

		/* clique direito*/
		$(document).on('click ', '#dir', function(){
			var aa= $('.mod figure.atual').index()-3;
			$('.atual div').css({"background-image":"url("+data.imagens[aa].imagem+")"})
			$('.proximo div').css({"background-image":"url("+data.imagens[aa+1].imagem+")"})
			$('.anterior div').css({"background-image":"url("+data.imagens[aa-1].imagem+")"})
		});

		/* clique esquerdo*/
		$(document).on('click ', '#esq', function(){
			var aa= $('.mod figure.atual').index()-3;

			$('.atual div').css({"background-image":"url("+data.imagens[aa].imagem+")"})
			$('.proximo div').css({"background-image":"url("+data.imagens[aa+1].imagem+")"})
			$('.anterior div').css({"background-image":"url("+data.imagens[aa-1].imagem+")"})
		});

		/* teclado*/
		document.addEventListener('keydown', function(e){

			if(e.keyCode==39){
				if($('.mod figure.atual').index()-2 < $('.mod figure').length ){
					var aa= $('.mod figure.atual').index()-3;
					$('.atual div').css({"background-image":"url("+data.imagens[aa].imagem+")"})
					$('.proximo div').css({"background-image":"url("+data.imagens[aa+1].imagem+")"})
					$('.anterior div').css({"background-image":"url("+data.imagens[aa-1].imagem+")"})
				}
			}
			if(e.keyCode==37){
				if($('.mod figure.atual').index()-2>=2){
					var aa= $('.mod figure.atual').index()-3;
					$('.atual div').css({"background-image":"url("+data.imagens[aa].imagem+")"})
					$('.proximo div').css({"background-image":"url("+data.imagens[aa+1].imagem+")"})
					$('.anterior div').css({"background-image":"url("+data.imagens[aa-1].imagem+")"})

				}
			}
		});


		if ($('.touch').length>0){

			document.getElementById('modal2').addEventListener('touchstart', function(e){
				
				touches.push(e.touches[0].clientX);
			});
			
			document.getElementById('modal2').addEventListener('touchend', function(e){
				touches.push(e.changedTouches[0].clientX);

				if (e.changedTouches[0].clientX==touches[touches.length-2]) {

						if ($('.atual FIGCAPTION').attr('class')!='hide') {

							var interval=setInterval(function(){

								$('FIGCAPTION').addClass('hide')
								$('FIGCAPTION').css({"bottom":"-50%"});
								$('#dir').css({"right":"-50%"})
								$('#esq').css({"left":"-50%"})
								clearInterval(interval);

							}, 500);
						
						}else{
							
							var interval=setInterval(function(){
							$('FIGCAPTION').removeClass('hide')
							$('FIGCAPTION').css({"bottom":"0%"});
							$('#dir').css({"right":"0%"})
							$('#esq').css({"left":"0%"})
							clearInterval(interval);

							}, 500);
						}
				};

				if((touches[touches.length-2]-touches[touches.length-1]) > 100){

					if($('.mod figure.atual').index()-2 < $('.mod figure').length ){

						if ($('.mod figure.atual').index()-2>=0) {

							$('.mod').find('figure.atual').removeClass('atual');
							$('.mod').find('figure.proximo').removeClass('proximo').addClass('atual');
							$('.mod').find('figure.enterior').removeClass('anterior').addClass('inativo');
							$('.mod').find('figure.atual').next('figure').removeClass().addClass('proximo');
							$('.mod').find('figure.atual').prev('figure').removeClass().addClass('anterior');
							
							omtrHitCounter('fotos', "img url: "+$('.mod').find('figure.atual img').attr('src'));
							window.ivc('trackPageView');

							if(	$('.mod figure.atual').index()-2 >= $('.mod').find('figure').length){

								$('#dir	').css({"display": "none"})
							}
							if(	$('.mod figure.atual').index()-2 >= 0){

								$('#esq').css({"display": "block"})
							}
						};
						
						var aa= $('.mod figure.atual').index()-3;

						$('.atual div').css({"background-image":"url("+data.imagens[aa].imagem+")"})
						$('.proximo div').css({"background-image":"url("+data.imagens[aa+1].imagem+")"})
						$('.anterior div').css({"background-image":"url("+data.imagens[aa-1].imagem+")"})
					}

				}else if((touches[touches.length-2]-touches[touches.length-1]) < -100){
					if($('.mod figure.atual').index()-2>=2){

						$('.mod').find('figure.atual').removeClass('atual');
						$('.mod').find('figure.proximo').removeClass('proximo').addClass('inativo');
						$('.mod').find('figure.anterior').removeClass('anterior').addClass('atual');
						$('.mod').find('figure.atual').prev('figure').removeClass().addClass('anterior');
						$('.mod').find('figure.atual').next('figure').removeClass().addClass('proximo');
						
						omtrHitCounter('fotos', "img url: "+$('.mod').find('figure.atual img').attr('src'));
						window.ivc('trackPageView');

						if(	$('.mod figure.atual').index()-2 <= $('.mod').find('figure').length){
						
							$('#dir	').css({"display": "block"})
						}

						if(	$('.mod figure.atual').index()-2 <= 1){

							$('#esq	').css({"display": "none"})
						}
					}

					var aa= $('.mod figure.atual').index()-3;

					$('.atual div').css({"background-image":"url("+data.imagens[aa].imagem+")"})
					$('.proximo div').css({"background-image":"url("+data.imagens[aa+1].imagem+")"})
					$('.anterior div').css({"background-image":"url("+data.imagens[aa-1].imagem+")"})
				}
				
				touches=[];
			});
		}
		content.appendChild(jsoon);
	}
/* FINAL - Carrega Conteúdo*/


/* INICIO - Galeria - Carrega Conteúdo*/
	function loadGal(a){
		var aa = a.id.split('imagejj')[1];
		$('.modoff').removeClass('modoff').addClass('mod');
		
		if (isMobile()) {

			$("#topofolha").css({"z-index":'999'})
		};

		if( parseInt(aa) >= $('.mod figure').length-1){

			$('#dir	').css({"display": "none"})
		}else{

			$('#dir	').css({"display": "block"})
		}

		if (parseInt(aa)>0) {

			$('#esq	').css({"display": "block"})
		}else{

			$('#esq	').css({"display": "none"})
		}

		$('.mod').find('figure').eq(aa).removeClass('inativo').addClass('atual');
		$('.mod').find('figure.atual').next().removeClass('inativo').addClass('proximo');
		$('.mod').find('figure.atual').prev().removeClass('inativo').addClass('anterior');
		

		omtrHitCounter('fotos', "img url: "+$('.mod').find('figure.atual img').attr('src'));
		window.ivc('trackPageView');
	}
/* FINAL - Galeria - Carrega Conteúdo*/



/* INTERAÇÃO */

	/* INICIO - INTERAÇÃO - Galeria - Botao Direito */
		$(document).on('click ', '#dir', function(){
			
			dir()
		});
	/* FINAL - INTERAÇÃO - Galeria - Botao Direito */

	/* INICIO - INTERAÇÃO - Galeria - Botao Esquerdo */
		$(document).on('click ', '#esq', function(){
			
			esq()
		});
	/* FINAL - INTERAÇÃO - Galeria - Botao Esquerdo */

	/* INICIO - INTERAÇÃO - Galeria - Botao Fechar */
		$(document).on('click ', '.close', function(){
			
			clos()
			$('.info-keys').css({"display":"none"});
			$('body').css({"overflow":"scroll"})
		});



	/* FINAL - INTERAÇÃO - Galeria - Botao Fechar */


	$( window ).resize(function() {
		if ($( window ).width()<=650) {
			
			$('.info-keys').css({"display":"none"});
		}

		if ($( window ).width()>=650 && $('#modal2').attr('class') == 'mod' ){

			$('.info-keys').css({"display":"block"});
		};
	})

	document.addEventListener('keydown', function(e){

		if(e.keyCode==39){
			if($('.mod figure.atual').index()-2 < $('.mod figure').length ){
				dir();
			}
		}

		if(e.keyCode==37){
			if($('.mod figure.atual').index()-2>=2){
				esq();
			}

		}
	});






(function(){
	$.ajax({
		url: url,
		dataType: 'jsonp',
		jsonpCallback: 'infoBack',
		contentType: 'text/plain',
		success: function(data) {
			loadData(data);

console.log("xd")

		}
	});
	var interval=setInterval(function(){
		if(document.readyState=='complete'){
			$('.loading').fadeOut(150);
			
			clearInterval(interval);
		}
	}, 500);
})();